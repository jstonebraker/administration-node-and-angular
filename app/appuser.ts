export class AppUser {

	constructor(
		public id: number,
		public username: string,
		public password: string
	) { }

}