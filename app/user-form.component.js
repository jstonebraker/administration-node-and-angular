System.register(['angular2/core', './appuser'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, appuser_1;
    var UserFormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (appuser_1_1) {
                appuser_1 = appuser_1_1;
            }],
        execute: function() {
            UserFormComponent = (function () {
                function UserFormComponent() {
                    this.active = true;
                    this.submitted = false;
                    this.model = new appuser_1.AppUser(0, '', '');
                }
                UserFormComponent.prototype.onSubmit = function () {
                    this.submitted = true;
                };
                UserFormComponent.prototype.newAppUser = function () {
                    var _this = this;
                    console.log("new app user...");
                    this.model = new appuser_1.AppUser(0, '', '');
                    this.active = false;
                    setTimeout(function () { return _this.active = true; }, 0);
                };
                Object.defineProperty(UserFormComponent.prototype, "diagnostic", {
                    get: function () {
                        return JSON.stringify(this.model);
                    },
                    enumerable: true,
                    configurable: true
                });
                UserFormComponent.prototype.stringify = function (obj) {
                    return JSON.stringify(obj);
                };
                UserFormComponent = __decorate([
                    core_1.Component({
                        selector: 'user-form',
                        templateUrl: 'app/user-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], UserFormComponent);
                return UserFormComponent;
            })();
            exports_1("UserFormComponent", UserFormComponent);
        }
    }
});
//# sourceMappingURL=user-form.component.js.map