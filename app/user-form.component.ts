import {Component} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {AppUser} from './appuser';

@Component({
    selector: 'user-form',
    templateUrl: 'app/user-form.component.html'
})
export class UserFormComponent {

	active = true;
	submitted = false;

	model = new AppUser(0, '', '');

	onSubmit() {
		this.submitted = true;
	}

	newAppUser() {
		console.log("new app user...");
		this.model = new AppUser(0, '', '');
		this.active = false;
		setTimeout(() => this.active = true, 0);
	}

	get diagnostic() {
		return JSON.stringify(this.model);
	}

	stringify(obj) {
		return JSON.stringify(obj);
	}
}