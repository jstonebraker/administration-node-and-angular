System.register([], function(exports_1) {
    var AppUser;
    return {
        setters:[],
        execute: function() {
            AppUser = (function () {
                function AppUser(id, username, password) {
                    this.id = id;
                    this.username = username;
                    this.password = password;
                }
                return AppUser;
            })();
            exports_1("AppUser", AppUser);
        }
    }
});
//# sourceMappingURL=appuser.js.map